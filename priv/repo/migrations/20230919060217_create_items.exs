defmodule Sarch.Repo.Migrations.CreateItems do
  use Ecto.Migration

  def change do
    create table(:items) do
      add :title, :text
      add :description, :text
      add :identifier, :string
      add :language, :string
      add :thumbnail, :text
      add :url, :text
      add :pdf, :text

      timestamps()
    end

    create unique_index(:items, [:identifier])
  end
end
