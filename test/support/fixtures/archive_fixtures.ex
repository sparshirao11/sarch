defmodule Sarch.ArchiveFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Sarch.Archive` context.
  """

  @doc """
  Generate a unique item identifier.
  """
  def unique_item_identifier, do: "some identifier#{System.unique_integer([:positive])}"

  @doc """
  Generate a item.
  """
  def item_fixture(attrs \\ %{}) do
    {:ok, item} =
      attrs
      |> Enum.into(%{
        description: "some description",
        title: "some title",
        identifier: unique_item_identifier(),
        url: "some url",
        language: "some language",
        thumbnail: "some thumbnail",
        pdf: "some pdf"
      })
      |> Sarch.Archive.create_item()

    item
  end
end
