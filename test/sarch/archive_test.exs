defmodule Sarch.ArchiveTest do
  use Sarch.DataCase

  alias Sarch.Archive

  describe "items" do
    alias Sarch.Archive.Item

    import Sarch.ArchiveFixtures

    @invalid_attrs %{
      description: nil,
      title: nil,
      identifier: nil,
      url: nil,
      language: nil,
      thumbnail: nil,
      pdf: nil
    }

    test "list_items/0 returns all items" do
      item = item_fixture()
      assert Archive.list_items() == [item]
    end

    test "get_item!/1 returns the item with given id" do
      item = item_fixture()
      assert Archive.get_item!(item.id) == item
    end

    test "create_item/1 with valid data creates a item" do
      valid_attrs = %{
        description: "some description",
        title: "some title",
        identifier: "some identifier",
        url: "some url",
        language: "some language",
        thumbnail: "some thumbnail",
        pdf: "some pdf"
      }

      assert {:ok, %Item{} = item} = Archive.create_item(valid_attrs)
      assert item.description == "some description"
      assert item.title == "some title"
      assert item.identifier == "some identifier"
      assert item.url == "some url"
      assert item.language == "some language"
      assert item.thumbnail == "some thumbnail"
      assert item.pdf == "some pdf"
    end

    test "create_item/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Archive.create_item(@invalid_attrs)
    end

    test "update_item/2 with valid data updates the item" do
      item = item_fixture()

      update_attrs = %{
        description: "some updated description",
        title: "some updated title",
        identifier: "some updated identifier",
        url: "some updated url",
        language: "some updated language",
        thumbnail: "some updated thumbnail",
        pdf: "some updated pdf"
      }

      assert {:ok, %Item{} = item} = Archive.update_item(item, update_attrs)
      assert item.description == "some updated description"
      assert item.title == "some updated title"
      assert item.identifier == "some updated identifier"
      assert item.url == "some updated url"
      assert item.language == "some updated language"
      assert item.thumbnail == "some updated thumbnail"
      assert item.pdf == "some updated pdf"
    end

    test "update_item/2 with invalid data returns error changeset" do
      item = item_fixture()
      assert {:error, %Ecto.Changeset{}} = Archive.update_item(item, @invalid_attrs)
      assert item == Archive.get_item!(item.id)
    end

    test "delete_item/1 deletes the item" do
      item = item_fixture()
      assert {:ok, %Item{}} = Archive.delete_item(item)
      assert_raise Ecto.NoResultsError, fn -> Archive.get_item!(item.id) end
    end

    test "change_item/1 returns a item changeset" do
      item = item_fixture()
      assert %Ecto.Changeset{} = Archive.change_item(item)
    end
  end
end
