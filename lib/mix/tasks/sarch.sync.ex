defmodule Mix.Tasks.Sarch.Sync do
  use Mix.Task

  def run(_args) do
    Mix.shell().info("Syncing from archive.org")
    {:ok, _} = Application.ensure_all_started(:req)
    {:ok, _} = Application.ensure_all_started(:sarch)

    Sarch.Sync.run()
  end
end
