defmodule Sarch.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      # Start the Telemetry supervisor
      SarchWeb.Telemetry,
      # Start the Ecto repository
      Sarch.Repo,
      # Start the PubSub system
      {Phoenix.PubSub, name: Sarch.PubSub},
      # Start Finch
      {Finch, name: Sarch.Finch},
      # Start the Endpoint (http/https)
      SarchWeb.Endpoint,
      # Start a worker by calling: Sarch.Worker.start_link(arg)
      # {Sarch.Worker, arg}
      Sarch.PeriodicSync
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Sarch.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @impl true
  def config_change(changed, _new, removed) do
    SarchWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
