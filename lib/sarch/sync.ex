defmodule Sarch.Sync do
  import Ecto.Query
  import Req

  @shortdoc "fetch files from archive.org and upsert into the database."

  @languages_map %{
    :english => ["English", "en", "english"],
    :urdu => ["ur", "Urdu", "urd", "urdu"],
    :hindi => ["hi", "hin", "hindi", "Hindi"]
  }

  defp fetch_identifiers() do
    url =
      "https://archive.org/advancedsearch.php?q=collection:csds-collection&fl[]=identifier&rows=1000&page=1&output=json"

    case Req.get(url) do
      {:ok, r} ->
        IO.puts("[INFO] Fetch identifiers successful")
        r.body["response"]["docs"] |> Enum.map(fn m -> m["identifier"] end)

      {:error, _r} ->
        IO.puts("Fetch failed")
    end
  end

  defp get_language(content) do
    language_param = content["metadata"]["language"] || content["metadata"]["ocr_detected_lang"]

    Enum.find(@languages_map, fn {key, list} ->
      Enum.member?(list, language_param)
    end)
    |> case do
      {key, _list} -> Atom.to_string(key)
      nil -> nil
    end
  end

  defp extract(content) do
    %{
      title: content["metadata"]["title"],
      created: content["created"],
      description: content["metadata"]["description"],
      identifier: content["metadata"]["identifier"],
      ocr_detected_lang: content["metadata"]["ocr_detected_lang"],
      ocr_detected_script: content["metadata"]["ocr_detected_script"],
      language: get_language(content),
      # collection: content["metadata"]["collection"],
      # d1: content["d1"],
      # d2: content["d2"],
      # dir: content["dir"],
      thumbnail: "https://#{content["d1"]}#{content["dir"]}/__ia_thumb.jpg",
      url: "https://archive.org/details/#{content["metadata"]["identifier"]}/",
      pdf: build_file_name(content, get_pdf_file_name(content))
    }
  end

  defp build_file_name(content, name) do
    "https://#{content["d1"]}#{content["dir"]}/#{name}"
  end

  defp get_pdf_file_name(content) do
    content["files"]
    |> Enum.find(%{}, fn file -> String.contains?(file["format"], ["pdf", "PDF"]) end)
    |> Map.get("name", "")
  end

  defp fetch_and_save(identifier) do
    url = "https://archive.org/metadata/#{identifier}"
    IO.puts("fetching #{identifier}")
    case Req.get(url) do
      {:ok, response} ->
        attrs = extract(response.body)
        IO.inspect(attrs)

        %Sarch.Archive.Item{}
        |> Sarch.Archive.Item.changeset(attrs)
        |> Sarch.Repo.insert(on_conflict: {:replace_all_except, [:id]}, conflict_target: :identifier)
        |> IO.inspect()

      {:error, response} ->
        IO.puts("ERROR Downloading #{identifier}: #{response.status}")
    end
  end

  defp get_existing_identifiers_mapset() do
    Sarch.Repo.all(from item in Sarch.Archive.Item, select: item.identifier)
    |> MapSet.new()
  end

  def run() do
    {:ok, _} = Application.ensure_all_started(:req)
    {:ok, _} = Application.ensure_all_started(:sarch)

    # fetch_identifiers()
    # |> MapSet.new()
    # |> MapSet.difference(get_existing_identifiers_mapset())
    # |> Enum.each(fn identifier ->
    #   fetch_and_save(identifier)
    # end)
    fetch_identifiers()
    |> Enum.each(fn identifier -> fetch_and_save(identifier) end)
  end
end
