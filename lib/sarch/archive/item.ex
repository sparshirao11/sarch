defmodule Sarch.Archive.Item do
  use Ecto.Schema
  import Ecto.Changeset

  schema "items" do
    field :description, :string
    field :title, :string
    field :identifier, :string
    field :url, :string
    field :language, :string
    field :thumbnail, :string
    field :pdf, :string

    timestamps()
  end

  @doc false
  def changeset(item, attrs) do
    item
    |> cast(attrs, [:title, :description, :identifier, :language, :thumbnail, :url, :pdf])
    |> validate_required([:title, :description, :identifier, :language, :thumbnail, :url, :pdf])
    |> unique_constraint(:identifier)
  end
end
