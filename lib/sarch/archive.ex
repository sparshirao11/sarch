defmodule Sarch.Archive do
  @moduledoc """
  The Archive context.
  """

  import Ecto.Query, warn: false
  alias Sarch.Repo

  alias Sarch.Archive.Item

  @doc """
  Returns the list of items.

  ## Examples

      iex> list_items()
      [%Item{}, ...]

  """
  def list_items do
    Repo.all(from i in Item, select: i, order_by: [asc: i.title])
  end

  def filter_items_by_language(language) when language in [:all, :english, :hindi, :urdu] do
    case Atom.to_string(language) do
      nil -> list_items()
      lang -> Repo.all(from i in Item, select: i, where: i.language == ^lang, order_by: [asc: i.title ])
    end
  end

  def get_count_by_language() do
    Repo.all(
      from i in Item,
      group_by: i.language,
      select: {i.language, count(i.id)}
    )
    |> Enum.map(fn {language, count} -> {String.to_existing_atom(language), count} end)
    |> Map.new
    |> Map.put(:all,  Sarch.Repo.aggregate(Sarch.Archive.Item, :count, :id))

  end
  @doc """
  Gets a single item.

  Raises `Ecto.NoResultsError` if the Item does not exist.

  ## Examples

      iex> get_item!(123)
      %Item{}

      iex> get_item!(456)
      ** (Ecto.NoResultsError)

  """
  def get_item!(id), do: Repo.get!(Item, id)

  def get_item_by_identifier!(identifier) do
    Item
    |> where(identifier: ^identifier)
    |> Repo.one!()
  end

  @doc """
  Creates a item.

  ## Examples

      iex> create_item(%{field: value})
      {:ok, %Item{}}

      iex> create_item(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_item(attrs \\ %{}) do
    %Item{}
    |> Item.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a item.

  ## Examples

      iex> update_item(item, %{field: new_value})
      {:ok, %Item{}}

      iex> update_item(item, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_item(%Item{} = item, attrs) do
    item
    |> Item.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a item.

  ## Examples

      iex> delete_item(item)
      {:ok, %Item{}}

      iex> delete_item(item)
      {:error, %Ecto.Changeset{}}

  """
  def delete_item(%Item{} = item) do
    Repo.delete(item)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking item changes.

  ## Examples

      iex> change_item(item)
      %Ecto.Changeset{data: %Item{}}

  """
  def change_item(%Item{} = item, attrs \\ %{}) do
    Item.changeset(item, attrs)
  end
end
