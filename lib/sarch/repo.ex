defmodule Sarch.Repo do
  use Ecto.Repo,
    otp_app: :sarch,
    adapter: Ecto.Adapters.Postgres
end
