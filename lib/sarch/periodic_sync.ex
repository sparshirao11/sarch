defmodule Sarch.PeriodicSync do
  use GenServer

  def start_link(_opts) do
    GenServer.start_link(__MODULE__, %{})
  end

  def init(state) do
    schedule_work(30 * 60 * 1000) # Schedule work to be performed at some point
    {:ok, state}
  end

  def handle_info(:work, state) do
    # Do the work you desire here

    Sarch.Sync.run()
    schedule_work() # Reschedule once more
    {:noreply, state}
  end

  defp schedule_work(interval \\ 60 * 60 * 1000)  do
    Process.send_after(self(), :work, interval)
  end
end
