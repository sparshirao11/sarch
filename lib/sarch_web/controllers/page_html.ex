defmodule SarchWeb.PageHTML do
  use SarchWeb, :html

  embed_templates "page_html/*"
end
