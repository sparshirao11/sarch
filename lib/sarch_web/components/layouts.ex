defmodule SarchWeb.Layouts do
  use SarchWeb, :html

  embed_templates "layouts/*"
end
