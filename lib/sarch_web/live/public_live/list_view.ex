defmodule SarchWeb.ListViewLive do
  use SarchWeb, :live_view

  alias Sarch.Archive
  alias Sarch.Archive.Item

  @impl true
  def mount(_params, _session, socket) do
    IO.inspect(language_map())
    {:ok,
     socket
     |> assign(:lang, :all)
     |> assign(:languages, language_map())
     |> stream(:items, Archive.list_items())}
  end

  defp language_map() do
    Map.merge(
      %{all: "All", english: "English", hindi: "Hindi", urdu: "Urdu"},
      Archive.get_count_by_language(),
      fn key, val1, val2 -> {val1, val2} end
      )
  end
  # @impl true
  # def handle_event("filter", %{"lang" => ""}, socket) do
  #   {:noreply,
  #    socket
  #    |> assign(lang: "")
  #    |> stream(:items, Archive.list_items())
  #   }

  # end

  @impl true
  def handle_event("filter", %{"lang" => lang}, socket) do
    {:noreply,
     socket
     |> assign(lang: lang |> String.to_existing_atom())
     |> stream(:items, Archive.filter_items_by_language(lang |> String.to_existing_atom()))}
  end
end

defmodule SarchWeb.ListFilterComponent do
  use SarchWeb, :live_component
end

defmodule SarchWeb.ItemShowComponent do
  use SarchWeb, :live_component

  def render(assigns) do
    ~H"""
    <div>blah</div>
    """
  end
end
