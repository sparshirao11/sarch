defmodule SarchWeb.PublicationsLive.Show do
  use SarchWeb, :live_view
  alias Sarch.Archive

  @impl true
  def mount(_params, _session, socket) do
    {:ok, socket}
  end

  @impl true
  def handle_params(%{"identifier" => identifier}, _, socket) do
    {:noreply,
     socket
     |> assign(:item, Archive.get_item_by_identifier!(identifier))}
  end
end
